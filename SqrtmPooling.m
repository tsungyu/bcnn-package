% BilinearPooling is the dagnn wapper of vl_nnbilinearpool which computes 
% self outer product of a feature output and pool the features across 
% all locations

% Copyright (C) 2015 Tsung-Yu Lin, Aruni RoyChowdhury, Subhransu Maji.
% All rights reserved.
%
% This file is part of the BCNN. It uses MATCONVNET package and is made 
% available under the terms of the BSD license (see the COPYING file).

classdef SqrtmPooling < dagnn.Filter
  properties
    method = 'eig'
    bpMethod = 'lyap'
    normalizeGradients = false;
    pow = 0.5;
    sigma = 1e-4;
    maxIter = 5;
  end

  methods
    function outputs = forward(obj, inputs, params)
        % params{1}: condition_sigma
        % inputs{1}: feature maps from previous layer
        % outputs{1}: sqrtm, outputs{2}: eigenvector of X'*X
        % outputs{3}: vector of eigenvalue of X'*X (no order
        % guaranteed)
        [outputs{1}, outputs{2}, outputs{3}] = vl_nnsqrtmpool(inputs{1}, obj.sigma, obj.pow, obj.method, [], obj.maxIter);
    end

    function [derInputs, derParams] = backward(obj, inputs, outputs, params, derOutputs) 
        % outputs{2}: eigenvectors of X'*X
        % outputs{3}: vector of eigenvalue of X'*X (no order
        % guaranteed)       
        derInputs{1} = vl_nnsqrtmpool(inputs{1}, obj.sigma, obj.pow, obj.bpMethod, outputs, obj.maxIter, derOutputs{1});
      if obj.normalizeGradients
          for i=1:numel(derInputs)
              gradNorm = sum(abs(derInputs{i}(:))) + 1e-8;
              derInputs{i} = derInputs{i}/gradNorm;
          end
      end
      derParams = {} ;
    end
    
    

    function backwardAdvanced(obj, layer)
      in = layer.inputIndexes ;
      out = layer.outputIndexes ;
      par = layer.paramIndexes ;
      net = obj.net ;

      inputs = {net.vars(in).value} ;
      outputs = {net.vars(out).value} ;
      derOutputs = {net.vars(out).der} ;
      for i = 1:numel(derOutputs)
        if isempty(derOutputs{i}), return ; end
      end

      if net.conserveMemory
        % clear output variables (value and derivative)
        % unless precious
        for i = out
          if net.vars(i).precious, continue ; end
          net.vars(i).der = [] ;
          net.vars(i).value = [] ;
        end
      end

      % compute derivatives of inputs and paramerters
      [derInputs, derParams] = obj.backward ...
        (inputs, outputs(2:end), {net.params(par).value}, derOutputs) ;
      if ~iscell(derInputs) || numel(derInputs) ~= numel(in)
        error('Invalid derivatives returned by layer "%s".', layer.name);
      end

      % accumuate derivatives
      for i = 1:numel(in)
        v = in(i) ;
        if net.numPendingVarRefs(v) == 0 || isempty(net.vars(v).der)
          net.vars(v).der = derInputs{i} ;
        elseif ~isempty(derInputs{i})
          net.vars(v).der = net.vars(v).der + derInputs{i} ;
        end
        net.numPendingVarRefs(v) = net.numPendingVarRefs(v) + 1 ;
      end

      for i = 1:numel(par)
        p = par(i) ;
        if (net.numPendingParamRefs(p) == 0 && ~net.accumulateParamDers) ...
              || isempty(net.params(p).der)
          net.params(p).der = derParams{i} ;
        else
          net.params(p).der = net.params(p).der + derParams{i} ;
        end
        net.numPendingParamRefs(p) = net.numPendingParamRefs(p) + 1 ;
      end
    end
    
    function rfs = getReceptiveFields(obj)
      rfs(1,1).size = [NaN NaN] ;
      rfs(1,1).stride = [NaN NaN] ;
      rfs(1,1).offset = [NaN NaN] ;
      rfs(2,1) = rfs(1,1) ;
    end

    function obj = SqrtmPooling(varargin)
      obj.load(varargin) ;
    end
  end
end

