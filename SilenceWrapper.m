classdef SilenceWrapper< dagnn.Layer
  properties
      fanIn = 1;
      blockType = 'dagnn.Layer';
      myBlock = [];
      params = {};
  end

  methods
    function outputs = forward(obj, inputs, params)
        outputs = obj.myBlock.forward(inputs(1:obj.fanIn), params);        
    end

    function [derInputs, derParams] = backward(obj, inputs, params, derOutputs)  
        [derInputs, derParams] = obj.myBlock.backward(inputs(1:obj.fanIn), params, derOutputs); 
        for i=obj.fanIn+1:numel(inputs)
            derInputs{i} = zeros(size(inputs{i})); % not used give a dummy output
        end
    end
    
    function rfs = getReceptiveFields(obj)
        rfs = obj.myBlock.getReceptiveFields();
    end

    function obj = SilenceWrapper(varargin)
      obj.load(varargin) ;
      fn = str2func(obj.blockType);
      obj.myBlock = fn(obj.params{:});
    end
  end
end

