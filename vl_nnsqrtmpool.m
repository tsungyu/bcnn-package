function [y, varargout] = vl_nnsqrtmpool(x, sigma, pow, method, outputs, maxIter, varargin)
% VL_NNBILINEARPOOL computes self outer product of a feature x and pool the features across 
% all locations
%
% Author: Subhransu Maji, Aruni RoyChowdhury, Tsung-Yu Lin

%
% This file is part of the BCNN and is made available under
% the terms of the BSD license (see the COPYING file).

% input:
% forward pass:
% x: input featre of size [hight, width, channels, batches]
% backward pass:
% x: input featre of size [hight, width, channels, batches]
% dzdy: the gradient with respect to output y.


% output:
% forward pass:
% y: self outer product of x.Features are pooled across locations
%    The output size if [1, 1, channels*channels, batches]
% backward pass:
% y: graident with respect to x. y will have the same size as input x.

backMode = numel(varargin) > 0 && ~isstr(varargin{1}) ;
if backMode
  dzdy = varargin{1};
  U = outputs{1};
  D = outputs{2};
end

gpuMode = isa(x, 'gpuArray');
[h, w, ch, bs] = size(x);

                

if backMode
    assert(pow==0.5, 'only supports backproping for pow=0.5');
    if gpuMode
        y = gpuArray(zeros(size(x), 'single'));
    else
        y = zeros(size(x), 'single');
    end

    % compute the svd if it is not computed in the forward pass
    if all(D(:) < eps)
        for b = 1:bs
            a = reshape(x(:,:,:,b), [h*w, ch]);
            tmp = (a'*a)./(h*w);
            [tempU, tempD] = eig(tmp);
            tempD = tempD + sigma*eye(size(tmp));
            
            % numerical stability
            tempD(tempD<0) = 0;
            
            D(:,b) = diag(tempD);
            U(:,:,b) = tempU;
        end
    end

    for b=1:bs
        dzdy_b = reshape(dzdy(1,1,:,b), [ch, ch]);
        switch method
            case 'svd'
                dzdy_b = sqrtSVD_backward(U(:,:,b), D(:,b), dzdy_b, sigma, ch);
            case 'lyap'
                dzdy_b = lyap_backward(U(:,:,b), D(:,b), dzdy_b);
            otherwise
                error('method should be one of {svd, lyap}');                
        end
        a = reshape(x(:,:,:,b), [h*w, ch]);
        y(:, :, :, b) = 2.*reshape(a*dzdy_b, [h, w, ch])/(h*w);
    end
else
    if gpuMode
        y = gpuArray(zeros([1, 1, ch*ch, bs], 'single'));
        eig_val = gpuArray(zeros([ch, bs], 'single'));
        eig_vec = gpuArray(zeros([ch, ch, bs], 'single'));
    else
        y = zeros([1, 1, ch*ch, bs], 'single');
        % each column is a vector of eigenvalues;
        eig_val = zeros([ch, bs], 'single');
        % bs matrices. Given the ith index along the third dimension, each column is an eigenvector of the  matrix
        eig_vec = zeros([ch, ch, bs], 'single');
    end
    for b = 1:bs,
        a = reshape(x(:,:,:,b), [h*w, ch]);
        
        switch method
            case 'schulz'
                tmp = (a'*a)./(h*w) + sigma*eye(size(a, 2));
                tmp = sqrt_schulz(tmp, maxIter);
            case 'db'
                tmp = (a'*a)./(h*w) + sigma*eye(size(a, 2));
                tmp = sqrt_db(tmp, maxIter);
            case 'sqrtm'
                tmp = (a'*a)./(h*w);
                
                assert(pow==0.5, 'sqrtm only supports for pow = 0.5');
                
                if gpuMode, tmp = gather(tmp); end
                
                tmp = sqrtm(tmp + sigma*eye(size(tmp)));
                tmp = real(tmp);
                
                if gpuMode, tmp = gpuArray(tmp); end
                
                % No output for eig_vec and eig_val (default zeros)
                
            case 'svd'
                tmp = (a'*a)./(h*w);
                [U, D] = eig(tmp);
                D = D + sigma*eye(size(tmp));
                
                % numerical stability
                D(D<0) = 0;
                
                if any(any(D<0))
                    keyboard;
                end
                eig_val(:,b) = diag(D);
                eig_vec(:,:,b) = U;
                
                D = diag(D).^pow;
                tmp = U*diag(D)*U';
                
            otherwise
                error('method should be one of {sqrtm, eig, svd}');
        end
        
        y(1,1,:, b) = reshape(tmp, [1 ch*ch]);
        varargout{1} = eig_vec;
        varargout{2} = eig_val;
    end
end

function dlda = lyap_backward(U, D, dldz)
dlda = mylyap(U, D, -dldz);

function X = mylyap(T, DA, C)
[ma, na] = size(DA);
if na==1
    DA = diag(DA);
end
DA = sqrt(DA);
IT = T';
DA = DA*ones(ma,ma);
X = -T*(IT*C*IT.'./(DA+DA.'))*T.';
X = real(X);


function dldx = sqrtSVD_backward(U, diagS, dldy, sigma, d)
% nnfspool is the function performing the forward pass of free shape pooling


if size(diagS, 2)~=1
    diagS = diag(S);
end

[dim, ~] = size(diagS);

% compute the eigenvectors
ind = (diagS-sigma) >(dim*eps(max(diagS)));
Dmin = sum(ind);


D = diag(diagS);

D = D(ind,ind); U = U(:,ind);
dldy_sym = .5*(dldy + dldy');

dldU = 2.*dldy_sym*U*diag(sqrt(diag(D)));
dldD = 0.5*diag(1./sqrt(diag(D)))*U'*dldy*U;

K = 1./(diag(D)*ones(1,Dmin)-(diag(D)*ones(1,Dmin))'); K(eye(size(K,1))>0)=0;

tmp = (K'.*(U'*dldU)) + diag(diag(dldD));

dldx = U*tmp*U';
dldx = 0.5*(dldx + dldx');


if any(any(isnan(dldx) | isinf(dldx)))
    dldx = zeros(d, d, 'like', U);
end


function sA = sqrt_db(A, maxIter)
Y_ = A; Z_ = eye(size(A,1));
for iter = 1:maxIter,
  Y1_ = (Y_ + inv(Z_))/2;
  Z_ = (Z_ + inv(Y_))/2;
  Y_ = Y1_;
end
sA = Y_;

function sA = sqrt_schulz(A, maxIter)
specScale = false;
if specScale
  B = A*A';
  R = max(sum(abs(B),2));
  scale = R/2;
else
  scale = sqrt(sum(sum(A.*A)));
end
Y_ = A/scale; 
Z_ = eye(size(A,1));
I3 = 3*eye(size(A,1));
for iter = 1:maxIter,
  T = (I3 - Z_*Y_)/2; 
  Y_ = Y_*T;
  Z_ = T*Z_;
end
sA = Y_*sqrt(scale);

  

