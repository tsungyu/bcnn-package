function [y, varargout] = vl_nngammademocratico2dp(x, p, iter, reg, varargin)

% input:
% forward pass:
% x: input featre of size [hight, width, channels, batches]
% p: gamma value for the gamma-democratic pooling
% iter: number of iteration for sinkhorn solver
% reg: the dampened facotr for the sinkhorn iteration

% backward pass:
% dzdy: the gradient with respect to output y.

% output:
% forward pass:
% y: self outer product of x at each pixel location. The output size if [hight, width, channels*channels, batches]

% backward pass:
% y: graident with respect to x. y will have the same size as input x.


backMode = numel(varargin) > 0 && ~isstr(varargin{1});
if backMode
    dzdy = varargin{1};
end

gpuMode = isa(x, 'gpuArray');
[h, w, ch, bs] = size(x);

if backMode
    if gpuMode
        y = gpuArray(zeros(size(x), 'single'));
    else
        y = zeros(size(x), 'single');
    end
else
    if gpuMode
        y = gpuArray(zeros([1, 1, ch*ch, bs], 'single'));
    else
        y = zeros([1, 1, ch*ch, bs], 'single');
    end

    d = size (x, 1);
    n = size (x, 2);

    for b=1:bs
        a = reshape(x(:,:,:,b), [h*w, ch]);
        G = a * a';
        G = G .^ 2;
        alpha = sinkhornm(G, p, reg, iter);
        alpha = alpha .^ 0.5;
        a = bsxfun(@times, a', alpha);
        y(:,:,:,b) = reshape((a * a') ./ (h*w), [1, ch*ch]);
    end

end

function lambda = sinkhornm(kn, p, reg, nbiter)

if ~exist ('reg'),    reg = 0.5; end
if ~exist ('nbiter'), nbiter = 10; end

n = size (kn, 1);
lambda = ones (1, size(kn, 1), 'single');
Ci = sum(kn, 2).^p;

for k=1:nbiter
    delta = (Ci'./sum(kn + 1e-10)).^reg;
    kn = bsxfun (@times, kn, delta);
    kn = bsxfun (@times, kn, delta');
    lambda = delta .* lambda;
end
