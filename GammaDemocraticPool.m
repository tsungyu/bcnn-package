
% GammaDemocraticPool is the dagnn wapper of vl_nngammademocratic 
% which aggreates first-order features with gamma-democratic pooling

% Copyright (C) 2018 Tsung-Yu Lin and Subhransu Maji.
% All rights reserved.
%
% This file is part of the BCNN and is made available under
% the terms of the BSD license (see the COPYING file).

classdef GammaDemocraticPool< dagnn.Filter
    properties
        iter = 10
        reg = 0.5
        p = 0.5
    end

    methods
        function outputs = forward(obj, inputs, params)
            outputs{1} = vl_nngammademocratic(inputs{1}, obj.p, obj.iter, obj.reg);
        end

        function [derInputs, derParams] = backward(obj, inputs, outputs, ...
                                            params, derOutputs)
            derInputs{1} = vl_nngammademocratic(inputs{1}, obj.p, obj.iter, ...
                                        obj.reg, derOutputs{1});
            derParams = {};
        end

        function getReceptiveFields(obj)
            rfs(1,1).size = [NaN NaN];
            rfs(1,1).stride = [NaN NaN];
            rfs(1,1).offset = [NaN NaN];
            rfs(2,1) = rfs(1,1);
        end

        function obj = GammaDemocraticPool(varargin)
            obj.load(varargin);
        end
    end
end
